#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct Page_table
{
    int PFI;
    int in_use;
    int present;

};

/*check physical frame is full or not*/
int phymem_is_full(int *frame,int frame_num)
{
    int i=0;
    for(i=0; i<frame_num; i++)
    {
        if(frame[i]==-1)return 0;
    }
    return 1;
}

/*check the target is in current list or not*/
int in_this_list(int* list,int list_num,int search)
{
    int i=0;
    for(i=0; i<list_num; i++)
    {
        if(list[i]==search)return 1;
    }
    return 0;
}

/*move the target to the head of list*/
int move_to_head(int* list,int list_num,int move_item)
{
    int i=0;
    for(i=0; i<list_num; i++)
    {
        if(list[i]==move_item)break;
    }

    for(i=i; i>0; i--)
    {
        list[i]=list[i-1];
    }
    list[0]=move_item;
}

/*move the target to the tail of list*/
int move_to_tail(int* list,int list_num,int move_item)
{
    int i=0;
    for(i=0; i<list_num; i++)
    {
        if(list[i]==move_item)break;
    }

    for(i=i; i<list_num-1; i++)
    {
        list[i]=list[i+1];
    }
    list[list_num-1]=move_item;
}


int main()
{
    char no_used[100];
    char type[10];
    char act[20];

    int page_num=0;
    int frame_num=0;
    int target=0;
    int i=0;

    //preprocess with input text
    scanf("%s",no_used);
    scanf("%s",type);

    scanf("%s",no_used);
    scanf("%s",no_used);
    scanf("%s",no_used);
    scanf("%s",no_used);
    scanf("%d",&page_num);

    scanf("%s",no_used);
    scanf("%s",no_used);
    scanf("%s",no_used);
    scanf("%s",no_used);
    scanf("%d",&frame_num);

    scanf("%s",no_used);
    fgets(no_used,100,stdin);

    int page[page_num];    //virtual page
    struct Page_table table[page_num];    //page table
    int disk[page_num];    //a storage not in memory
    for(i=0; i<page_num; i++)
    {
        page[i]=-1;
        table[i].in_use=0;
        table[i].PFI=-1;
        table[i].present=0;
        disk[i]=-1;
    }

    int frame[frame_num];   //physical frame
    int reference[frame_num];   //reference of physical frame
    int dirty[frame_num];   //check the data has been touched or not
    for(i=0; i<frame_num; i++)
    {
        frame[i]=-1;
        reference[i]=-1;
        dirty[i]=-1;
    }

    /*start page replacement*/

    //Method1---FIFO
    if(strcmp(type,"FIFO")==0)      //use first in first out
    {
        char num[10];
        int num_i=0;
        int time_counter=0;
        float tot=0;
        float hit=0;

        while(1)
        {
            time_counter++;
            memset(num,0,sizeof(num));
            if(fgets(act,10,stdin)==NULL)break;
            for(i=0; i<sizeof(act); i++)
            {
                if(act[i]==' ')break;
            }

            int j=0;
            while(1)
            {
                i++;
                if(act[i]=='\n')break;
                num[j]=act[i];
                j++;
            }
            num_i=atoi(num);

            //search in page_table
            if(table[num_i].in_use==1 && table[num_i].present==1)      //current page in page table
            {
                printf("Hit, %d=>%d\n",num_i,table[num_i].PFI);
                tot++;
                hit++;
            }

            if(table[num_i].in_use==0 || table[num_i].present==0)      //current page not in page table
            {
                //check whether physical memory is full
                if(phymem_is_full(frame,frame_num))         //if physical memory is full , swap
                {
                    //put swaped page back to disk
                    for(i=0; i<page_num; i++)
                    {
                        if(disk[i]==-1)
                        {
                            int swaped=__INT_MAX__;
                            int first=0;
                            for(j=0; j<frame_num; j++)
                            {
                                if(reference[j]<swaped)
                                {
                                    swaped=reference[j];
                                    first=j;
                                }
                            }

                            disk[i]=frame[first];

                            for(j=0; j<page_num; j++)
                            {
                                if(table[j].PFI==first)
                                {
                                    table[j].PFI=-1;
                                    table[j].present=0;
                                    break;
                                }
                            }

                            printf("Miss, %d, %d>>%d, ",first,frame[first],i);
                            tot++;

                            //get page from disk
                            if(table[num_i].in_use==1 && table[num_i].present==0)
                            {
                                for(i=0; i<page_num; i++)
                                {
                                    if(disk[i]==num_i)
                                    {
                                        //frame[first]=disk[i];
                                        disk[i]=-1;
                                        break;
                                    }
                                }
                            }

                            frame[first]=num_i;
                            reference[first]=time_counter;
                            table[num_i].PFI=first;
                            if(table[num_i].in_use==0)
                            {
                                printf("%d<<-1\n",num_i);
                            }
                            else
                            {
                                printf("%d<<%d\n",num_i,i);
                            }
                            table[num_i].in_use=1;
                            table[num_i].present=1;

                            break;
                        }
                    }
                }

                //if physical memory is not full , put in smallest number free frame
                if(!phymem_is_full(frame,frame_num))
                {
                    for(i=0; i<frame_num; i++)
                    {
                        if(frame[i]==-1)
                        {
                            frame[i]=num_i;
                            reference[i]=time_counter;
                            table[num_i].in_use=1;
                            table[num_i].PFI=i;
                            table[num_i].present=1;
                            break;
                        }
                    }
                    printf("Miss, %d, -1>>-1, %d<<-1\n",i,num_i);
                    tot++;
                }
            }

        }
        printf("Page Fault Rate: %f\n",1-hit/tot);
    }

    //Method2---ESCA
    if(strcmp(type,"ESCA")==0)      //use enhance second chance algorithm
    {
        char behavior[10];
        char num[10];
        int num_i=0;
        int time_counter=0;
        float tot=0;
        float hit=0;

        while(1)
        {
            time_counter++;
            memset(num,0,sizeof(num));
            memset(behavior,0,sizeof(behavior));
            if(fgets(act,10,stdin)==NULL)break;
            for(i=0; i<sizeof(act); i++)
            {
                if(act[i]==' ')break;
                behavior[i]=act[i];
            }

            int j=0;
            while(1)
            {
                i++;
                if(act[i]=='\n')break;
                num[j]=act[i];
                j++;
            }
            num_i=atoi(num);

            //search in page table
            if(table[num_i].present==1)
            {
                if(strcmp(behavior,"Write"))dirty[table[num_i].PFI]=1;

                printf("Hit, %d=>%d\n",num_i,table[num_i].PFI);
                tot++;
                hit++;
            }

            if(table[num_i].in_use==0 ||table[num_i].present==0)
            {
                if(phymem_is_full(frame,frame_num))         //physical memory is full , swap
                {
                    //check reference bit
                    i=0;
                    int count=0;
                    while(1)
                    {
                        if(count%2==0)
                        {
                            if(reference[i]==0 && dirty[i]==0)break;
                        }
                        if(count%2==1)
                        {
                            if(reference[i]==0 && dirty[i]==1)break;
                            reference[i]=0;
                        }
                        i++;
                        if(i==frame_num)
                        {
                            count++;
                            i=0;
                        }
                    }

                    //send swaped page back to disk
                    for(j=0; j<page_num; j++)
                    {
                        if(disk[j]==-1)
                        {
                            disk[j]=frame[i];
                            break;
                        }
                    }

                    //reset page table
                    int k=0;
                    for(k=0; k<page_num; k++)
                    {
                        if(table[k].PFI==i)
                        {
                            table[k].PFI=-1;
                            table[k].present=0;
                            break;
                        }
                    }

                    printf("Miss, %d, %d>>%d, ",i,k,j);
                    tot++;

                    //get page from disk
                    if(table[num_i].in_use==1 && table[num_i].present==0)
                    {
                        for(j=0; j<page_num; j++)
                        {
                            if(disk[j]==num_i)
                            {
                                disk[j]=-1;
                                break;
                            }
                        }
                    }

                    frame[i]=num_i;
                    reference[i]=1;
                    dirty[i]=0;
                    if(strcmp(behavior,"Write"))dirty[i]=1;

                    if(table[num_i].in_use==0)
                    {
                        printf("%d<<-1\n",num_i);
                    }
                    else
                    {
                        printf("%d<<%d\n",num_i,j);
                    }
                    table[num_i].in_use=1;
                    table[num_i].PFI=i;
                    table[num_i].present=1;
                }

                if(!phymem_is_full(frame,frame_num))         //physical memory is not full , put in smallest number free frame
                {
                    for(i=0; i<frame_num; i++)
                    {
                        if(frame[i]==-1)
                        {
                            frame[i]=num_i;
                            reference[i]=1;
                            dirty[i]=0;
                            if(strcmp(behavior,"Write"))dirty[i]=1;

                            table[num_i].in_use=1;
                            table[num_i].PFI=i;
                            table[num_i].present=1;
                            break;
                        }
                    }
                    printf("Miss, %d, -1>>-1, %d<<-1\n",i,num_i);
                    tot++;
                }
            }
        }
        printf("Page Fault Rate: %f\n",1-hit/tot);
    }

    //Method3---SLRU
    if(strcmp(type,"SLRU")==0)
    {
        char behavior[10];
        char num[10];
        int num_i=0;
        int time_counter=0;
        float tot=0;
        float hit=0;

        int active_num=frame_num/2;
        int inactive_num=frame_num/2+(frame_num%2);

        int active[active_num];
        int inactive[inactive_num];
        //int active_reference[active_num];

        //initial frame and reference
        for(i=0; i<inactive_num; i++)
        {
            inactive[i]=-1;
            //inactive_reference[i]=-1;
        }
        for(i=0; i<active_num; i++)
        {
            active[i]=-1;
            //active_reference[i]=-1;
        }

        while(1)
        {
            time_counter++;
            memset(num,0,sizeof(num));
            memset(behavior,0,sizeof(behavior));
            if(fgets(act,10,stdin)==NULL)break;
            for(i=0; i<sizeof(act); i++)
            {
                if(act[i]==' ')break;
                behavior[i]=act[i];
            }

            int j=0;
            while(1)
            {
                i++;
                if(act[i]=='\n')break;
                num[j]=act[i];
                j++;
            }
            num_i=atoi(num);

            //search in page table
            if(table[num_i].present==1)
            {
                //if page in physical memory

                //if page in active list
                if(in_this_list(active,active_num,num_i))
                {
                    move_to_head(active,active_num,num_i);
                    reference[table[num_i].PFI]=1;
                }

                //if page in inactive list
                else
                {
                    //if reference bit is 0
                    if(reference[table[num_i].PFI]==0)
                    {
                        move_to_head(inactive,inactive_num,num_i);
                        reference[table[num_i].PFI]=1;
                    }

                    //if reference bit is 1
                    else
                    {
                        reference[table[num_i].PFI]=0;

                        //move to active list

                        //if active list is full , refill
                        if(phymem_is_full(active,active_num))
                        {
                            i=active_num-1;
                            //find swaped page
                            while(1)
                            {
                                if(reference[table[active[i]].PFI]==0)break;
                                reference[table[active[i]].PFI]=0;
                                //i++;
                                //if(i=active_num)i=0;
                                move_to_head(active,active_num,active[i]);
                            }

                            for(j=0; j<inactive_num; j++)
                            {
                                if(inactive[j]==num_i)
                                {
                                    inactive[j]=active[i];
                                    move_to_head(inactive,inactive_num,inactive[j]);
                                    break;
                                }
                            }

                            active[i]=num_i;
                            move_to_head(active,active_num,num_i);
                        }

                        //if active list is not full , just put in
                        else
                        {
                            for(i=0; i<active_num; i++)
                            {
                                if(active[i]==-1)
                                {
                                    active[i]=num_i;
                                    move_to_head(active,active_num,num_i);
                                    break;
                                }
                            }

                            for(j=0; j<inactive_num; j++)
                            {
                                if(inactive[j]==num_i)
                                {
                                    inactive[j]=-1;
                                    break;
                                }
                            }
                        }
                    }
                }
                printf("Hit, %d=>%d\n",num_i,table[num_i].PFI);
                tot++;
                hit++;
            }

            //if page not in physical memory
            if(table[num_i].in_use==0 || table[num_i].present==0)
            {

                //if physical frame is full , swap
                if(phymem_is_full(frame,frame_num))
                {
                    //find evicted page
                    i=inactive_num-1;
                    while(1)
                    {
                        if(reference[table[inactive[i]].PFI]==0)break;
                        reference[table[inactive[i]].PFI]=0;
                        //i++;
                        //if(i==inactive_num)i=0;
                        move_to_head(inactive,inactive_num,inactive[i]);
                    }

                    //send evicted page into disk
                    for(j=0; j<page_num; j++)
                    {
                        if(disk[j]==-1)
                        {
                            disk[j]=frame[table[inactive[i]].PFI];
                            break;
                        }
                    }

                    printf("Miss1, %d, %d>>%d, ",table[inactive[i]].PFI,inactive[i],j);
                    tot++;

                    //get page from disk
                    int k=0;
                    if(table[num_i].in_use==1 && table[num_i].present==0)
                    {
                        for(k=0; k<page_num; k++)
                        {
                            if(disk[k]==num_i)
                            {
                                disk[k]=-1;
                                break;
                            }
                        }
                    }

                    frame[table[inactive[i]].PFI]=num_i;
                    reference[table[inactive[i]].PFI]=1;

                    if(table[num_i].in_use==0)
                    {
                        printf("%d<<-1\n",num_i);
                    }
                    else
                    {
                        printf("%d<<%d\n",num_i,k);
                    }
                    table[num_i].in_use=1;
                    table[num_i].PFI=table[inactive[i]].PFI;
                    table[num_i].present=1;

                    table[inactive[i]].PFI=-1;
                    table[inactive[i]].present=0;
                    inactive[i]=num_i;
                    move_to_head(inactive,inactive_num,num_i);
                }

                //if physical memory is not full
                else
                {
                    //if inactive list is full ,swap
                    if(phymem_is_full(inactive,inactive_num) && inactive_num>0)
                    {
                        j=inactive_num-1;
                        while(1)
                        {
                            if(reference[table[inactive[j]].PFI]==0)
                            {
                                break;
                            }
                            reference[table[inactive[j]].PFI]=0;
                            move_to_head(inactive,inactive_num,inactive[j]);
                        }

                        int k=0;
                        for(k=0; k<page_num; k++)
                        {
                            if(disk[k]==-1)
                            {
                                disk[k]=frame[table[inactive[j]].PFI];
                                break;
                            }
                        }

                        //get page from disk
                        int l=-1;
                        if(table[num_i].in_use==1 && table[num_i].present==0)
                        {
                            for(l=0; l<page_num; l++)
                            {
                                if(disk[l]==num_i)
                                {
                                    disk[l]=-1;
                                    break;
                                }
                            }
                        }

                        printf("Miss3, %d, %d",inactive[j],frame[table[inactive[j]].PFI]);
                        tot++;

                        table[num_i].PFI=table[inactive[j]].PFI;
                        table[num_i].in_use=1;
                        table[num_i].present=1;

                        frame[table[inactive[j]].PFI]=num_i;
                        reference[table[inactive[j]].PFI]=1;

                        table[inactive[j]].PFI=-1;
                        table[inactive[j]].present=0;
                        inactive[j]=num_i;
                        move_to_head(inactive,inactive_num,num_i);

                        printf(">>%d, %d<<%d\n",k,num_i,l);
                    }

                    //if inactive list is not full
                    else
                    {
                        //get page from disk
                        int l=-1;
                        if(table[num_i].in_use==1 && table[num_i].present==0)
                        {
                            for(l=0; l<page_num; l++)
                            {
                                if(disk[l]==num_i)
                                {
                                    disk[l]=-1;
                                    break;
                                }
                            }
                        }

                        for(i=0; i<frame_num; i++)
                        {
                            if(frame[i]==-1)break;
                        }

                        for(j=0; j<inactive_num; j++)
                        {
                            if(inactive[j]==-1)
                            {
                                inactive[j]=num_i;
                                move_to_head(inactive,inactive_num,num_i);
                                break;
                            }
                        }

                        table[num_i].in_use=1;
                        table[num_i].PFI=i;
                        table[num_i].present=1;

                        frame[i]=num_i;
                        reference[i]=1;

                        printf("Miss4, %d, -1>>-1, %d<<-1\n",i,num_i);
                        tot++;
                    }
                }
            }
        }
        printf("Page Fault Rate: %f",1-hit/tot);
    }

}